import { Expediente } from "../models/expediente";
import { ItemList } from '../models/ItemList';
import { ServicioHotel } from '../models/ServicioHotel';
import { ServicioMiViaje } from '../models/ServicioMiViaje';
import { ServicioVuelo } from '../models/ServicioVuelo';
import { Observable, of } from 'rxjs';
import { TipoServicio } from '../models/TipoServicio';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ExpedienteService {


    public Expedientes : Array<Expediente>

    constructor(){
        var empresa: ItemList = { Id: "014", Descripcion: "Multidestinos" };

        var serviciohotel: ServicioHotel = {
            Destino: "Phuket",
            Direccion: "Agassoo sgahjafshsg ",
            FechaInicio: new Date,
            FechaFin: new Date,
            Regimen: "Pension copleta",
            IdServicio: "123234456",
            Foto: "https://t-ec.bstatic.com/images/hotel/max1024x768/122/122930638.jpg",
            Noches: "15",
            PasajeroPrincipal: { Nombre: "Antonio", Apellidos: "Paredes Picon", Email: "aparedes@ofi.es", NivelEdadPax: "Adulto", PasajeroPrincipal:"S" },
            Pasajeros: [{ Nombre: "Antonio", Apellidos: "Paredes Picon", Email: "aparedes@ofi.es", NivelEdadPax: "Adulto", PasajeroPrincipal: "S" }],
            Titulo: "The Marina Hotel Phuket",
            Rooming: null,
            Tipo:  TipoServicio.Hotel
            
        }

        var serviciovuelo: ServicioVuelo = {
            Origen:"Madrid",
            Destino: "Phuket",
            FechaInicio: new Date,
            FechaFin: new Date,
            IdServicio: "123234455",
            Foto: "https://www.viajejet.com/wp-content/viajes/Ventajas-y-desventajas-de-comprar-un-vuelo-barato.jpg",
            PasajeroPrincipal: { Nombre: "Antonio", Apellidos: "Paredes Picon", Email: "aparedes@ofi.es", NivelEdadPax: "Adulto", PasajeroPrincipal: "S" },
            Pasajeros: [{ Nombre: "Antonio", Apellidos: "Paredes Picon", Email: "aparedes@ofi.es", NivelEdadPax: "Adulto", PasajeroPrincipal: "S" }],
            Titulo: "Vuelo Madrid Pucket",
            Tipo: TipoServicio.Vuelo
            
        }

        var servicios: ServicioMiViaje[] = [
            serviciohotel, serviciovuelo
        ]

        var expediente: Expediente = {
            Id: '014-U-102345',
            Empresa: empresa,
            Servicios: servicios,
            CodExpediente: "102345",
            FechaInicio: new Date(),
            FechaFin: new Date(),
            Titulo: "Triangulo del Oro",
            Descripcion: "A pesar de no gozar de la popularidad con la que cuenta su hermana mayor, Chiang Mai, Chiang Rai es una de las ciudades del norte de Tailandia con mas encanto. La mayor parte de los viajeros paran en ella en su camino hacia Laos, para coger el popular slowboat que surca el Mekong hasta su pais vecino. No obstante, aquellos que deciden quedarse un par de dias en ella descubriran que Chiang Rai es mas que un lugar de paso repleto de casas de cambio.",
            Localizador: "102345h",
            Personas: "4",
            Itinerario: null,
            EstadoBajada: null,
            Foto: "https://reservascms.delunoalotroconfin.com/Content/images/000/Productos/Prod_567_1.jpg"
        }

        var expdiente2 = {...expediente}
        var expdiente3 = {...expediente}
        var expdiente4 = {...expediente}
        var expdiente5 = {...expediente}

        expdiente2.Titulo = "Grandes templos"
        expdiente2.Id = "014-U-102344"
        expdiente2.Foto = "https://reservascms.delunoalotroconfin.com/Content/images/000/Productos/Prod_1516_1.jpg"

        
        expdiente3.Titulo = "Piraguismo en Phuket"
        expdiente3.Id = "014-U-102343"
        expdiente3.Foto = "https://reservascms.delunoalotroconfin.com/Content/images/000/Productos/Prod_443_1.jpg"

        expdiente4.Titulo = "Profundidades de Thailandia"
        expdiente4.Id = "014-U-102342"
        expdiente4.Foto = "https://reservascms.delunoalotroconfin.com/Content/images/000/Productos/Prod_3060_1.jpg"

        expdiente5.Titulo = "Compro oro"
        expdiente5.Id = "014-U-102341"
        expdiente5.Foto = "https://reservascms.delunoalotroconfin.com/Content/images/000/Productos/Prod_1513_1.jpg"

        this.Expedientes = [expediente, expdiente3, expdiente2,  expdiente4, expdiente5]
    }

    dameExpediente(id: string): Observable<Expediente> {
        var expediente = this.Expedientes.find( elemento  => elemento.Id === id) 
        return of(expediente);

    }


    dameServicio(idExpediente: string, idServicio: string): Observable<ServicioMiViaje> {
        var expediente = this.Expedientes.find(expediente => expediente.Id === idExpediente)
        var servicio = expediente.Servicios.find(servicio => servicio.IdServicio == idServicio)
        return of(servicio);
    }


    dameExpedientes() : Observable<Expediente[]> {
        return of(this.Expedientes);
    }
}