import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './tabs.page';

const routes: Routes = [

    {
        path: 'login',
        children: [
            {
                path: '',
                loadChildren: '../login/login.module#LoginPageModule'
            }
        ]
    },
    {
    path: 'tabs',
    component: HomePage,
    children: [
      
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: '../tab2/tab2.module#Tab2PageModule'
          }
        ]
      },
      {
        path: 'tab3',
        children: [
            {
              path: '',
              loadChildren: '../tab3/tab3.module#Tab3PageModule'
            }
          ]
        },
        
        {
          path: 'reservas',
          children: [
              {
                  path: '',
                  loadChildren: '../reservas/reservas.module#ReservasModule'
              },
              {
                  path: ':idReserva',
                  children: [
                      {
                          path: '',
                          loadChildren: '../reserva/reserva.module#ReservaModule'
                      },
                      {
                          path: 'servicio/:idServicio',
                          loadChildren: '../servicio/servicio.module#ServicioModule'
                      }

                  ]
              },
          ]
      },
      
    ]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {}
