import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class HomePage {


    constructor(public router: Router, public oauthService: OAuthService) {
        //if (!this.oauthService.hasValidIdToken()) {
        //    this.router.navigate(['/login']);
        //}
    }

    logout() {
        this.oauthService.logOut(true);
        this.router.navigate(['/login']);
    }

    get givenName() {
        const claims: any = this.oauthService.getIdentityClaims();
        if (!claims) {
            return null;
        }
        return claims.name;
    }

    get claims() {
        return this.oauthService.getIdentityClaims();
    }
}
