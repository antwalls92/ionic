import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExpedienteService } from '../services/expedienteService';
import { ServicioMiViaje } from '../models/ServicioMiViaje';
import { TipoServicio } from '../models/TipoServicio';

@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.page.html',
  styleUrls: ['./servicio.page.scss'],
})
export class ServicioPage implements OnInit {

    servicio: ServicioMiViaje;
    TiposServicio = TipoServicio

    constructor(private expedienteService: ExpedienteService, private route: ActivatedRoute) { }

    ngOnInit() {

        this.route.params.subscribe(params => {

            var idReserva = params['idReserva'];
            var idServicio = params['idServicio'];
            this.expedienteService
                .dameServicio(idReserva, idServicio)
                .subscribe(
                (_servicio: ServicioMiViaje) => (this.servicio = _servicio)
                )
        });
    }

    formatDate(date: Date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

}
