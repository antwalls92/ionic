import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ServicioPage } from './servicio.page';

@NgModule({
    declarations: [ServicioPage],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        CommonModule,
        RouterModule.forChild([{ path: '', component: ServicioPage }])

  ]
})
export class ServicioModule { }
