﻿import { ServicioMiViaje } from './ServicioMiViaje';
import { TipoServicio } from "./TipoServicio";
export class ServicioVuelo extends ServicioMiViaje {
    Tipo: TipoServicio.Vuelo;
    Origen: string;
    Destino: string;
}
