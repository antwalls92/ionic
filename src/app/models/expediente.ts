import { ItemList } from './ItemList';
import { Itinerario } from './Itinerario';
import { ServicioMiViaje } from './ServicioMiViaje';

export class Expediente {

    Id : string
    Empresa: ItemList 
    Titulo: string
    Descripcion: string
    FechaInicio: Date
    FechaFin: Date
    Servicios: ServicioMiViaje[] 
    Localizador : string
    Personas : string
    CodExpediente :string
    EstadoBajada: ItemList 
    Itinerario: Itinerario 
    Foto: string

}

