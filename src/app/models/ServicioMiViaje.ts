﻿import { InfoPasajero } from './InfoPasajero';
import { TipoServicio } from "./TipoServicio";
export abstract class ServicioMiViaje {
    abstract Tipo: TipoServicio;
    IdServicio: string;
    Titulo: string;
    FechaInicio: Date;
    FechaFin: Date;
    Foto: string;
    PasajeroPrincipal: InfoPasajero;
    Pasajeros: Array<InfoPasajero>;
}
