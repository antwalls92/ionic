﻿export class DiaItinerario {
    Destino: string;
    NumServicio: string;
    Descripcion: string;
    Titulo: string;
    Cabecera: string;
    Dia: string;
}
