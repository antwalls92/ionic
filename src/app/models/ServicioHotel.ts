﻿import { ServicioMiViaje } from './ServicioMiViaje';
import { TipoServicio } from "./TipoServicio";
export class ServicioHotel extends ServicioMiViaje {
    Tipo: TipoServicio.Hotel;
    Noches: string;
    Direccion: string;
    Regimen: string;
    Rooming;
    Destino: string;
}
