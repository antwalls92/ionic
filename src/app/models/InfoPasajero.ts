﻿export abstract class InfoPasajero {
    Nombre: string;
    Apellidos: string;
    PasajeroPrincipal: string;
    NivelEdadPax: string;
    Email: string;
}
