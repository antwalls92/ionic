import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Facebook, Google, OauthCordova } from 'ionic-cordova-oauth';
import { HomePage } from './tabs/tabs.page';
import { LoginPage } from './login/login.page';
import { OAuthService } from 'angular-oauth2-oidc'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

    rootPage: any = HomePage;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private oauthService: OAuthService

  ) {
      this.initializeApp();
  }

    initializeApp() {

        if (this.oauthService.hasValidIdToken()) {
            this.rootPage = HomePage;
        } else {
            this.rootPage = LoginPage;
        }

        // Load Discovery Document and then try to login the user
        this.oauthService.loadDiscoveryDocument().then(() => {
            this.oauthService.tryLogin();
        });

        this.platform.ready().then(() => {
          this.statusBar.styleDefault();
            this.splashScreen.hide();

        });
  }
}
