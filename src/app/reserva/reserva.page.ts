import { Component, OnInit } from '@angular/core';
import { Expediente } from '../models/expediente';
import { ExpedienteService } from '../services/expedienteService';
import { TipoServicio } from '../models/TipoServicio';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-reservapage',
  templateUrl: './reserva.page.html',
  styleUrls: ['./reserva.page.scss'],
})
export class ReservaPage implements OnInit {

    expediente: Expediente
    TiposServicio = TipoServicio
    tab: string

    constructor(private expedienteService: ExpedienteService, private route: ActivatedRoute) { }

    ngOnInit() {

       this.route.params.subscribe(params => {

           var idReserva = params['idReserva']; 
           this.expedienteService
               .dameExpediente(idReserva)
               .subscribe(
                   (_expediente: Expediente) => (this.expediente = _expediente)
               )
        });
    }

    segmentChanged(event) {
        this.tab = event.detail.value;
    }

    formatDate(date: Date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

}
