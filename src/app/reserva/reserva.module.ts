import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReservaPage } from './reserva.page';

@NgModule({
    declarations: [ReservaPage],
  imports: [
      IonicModule,
      CommonModule,
      FormsModule,
      RouterModule.forChild([{ path: '', component: ReservaPage }])
  ]
})
export class ReservaModule { }
