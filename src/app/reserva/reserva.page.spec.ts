import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservapagePage } from './reservapage.page';

describe('ReservapagePage', () => {
  let component: ReservapagePage;
  let fixture: ComponentFixture<ReservapagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservapagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservapagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
