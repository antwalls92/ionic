import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservasPage } from './reservas.page';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ReservasPage],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: ReservasPage }])
  ]
})
export class ReservasModule { }
