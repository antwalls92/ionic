import { Component, OnInit } from '@angular/core';
import { Expediente } from '../models/expediente';
import { ExpedienteService } from '../services/expedienteService';



@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.page.html',
  styleUrls: ['./reservas.page.scss'],
})
export class ReservasPage implements OnInit {

  expedientes: Array<Expediente>
  constructor(private expedienteService: ExpedienteService) { }

  ngOnInit() {
    this.expedienteService
    .dameExpedientes()
    .subscribe(
        ( _expedientes: Array<Expediente>) => (this.expedientes = _expedientes)
    )
  }
}
